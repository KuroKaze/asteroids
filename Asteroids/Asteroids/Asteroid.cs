﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Asteroids
{
    class Asteroid : Entity
    {
        public float speed, rotate;

        public Asteroid(Texture2D texture, int numRows, int numColumns, int startFrame, int endFrame, Vector2 loc, float scl, float deg)
            : base(texture, numRows, numColumns, startFrame, endFrame, loc, scl, deg)
        {
            angle = MathHelper.ToRadians(deg);
            rotate = angle;
        }

        public Asteroid(Entity ent)
            : base(ent)
        {

        }
        
        public void init(float spd)
        {
            speed = spd;
        }

        public override void Update(GameTime gameTime)
        {
            //if(gameTime.TotalGameTime.Milliseconds % 25 == 0)
                angle += MathHelper.ToRadians(5.0f);
            transformToward(speed, rotate);
        }
    }
}
