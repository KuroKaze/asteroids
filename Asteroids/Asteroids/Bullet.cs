﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Asteroids
{
    class Bullet : Entity
    {

        public float speed, travelDis;

        public Bullet(Texture2D texture, int numRows, int numColumns, int startFrame, int endFrame, Vector2 loc, float scl, float deg)
            : base(texture, numRows, numColumns, startFrame, endFrame, loc, scl, deg)
        {

        }

        public void init(float spd)
        {
            speed = spd;
        }

        public override void Update(GameTime gameTime)
        {
            transformToward(speed, angle);
            travelDis += speed;
        }
    }
}
