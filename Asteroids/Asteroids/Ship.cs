﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Asteroids
{
    class Ship : Entity
    {
        List<float> speeds, angles;
        public Ship(Texture2D texture, int numRows, int numColumns, int startFrame, int endFrame, Vector2 loc)
            : base(texture, numRows, numColumns, startFrame, endFrame, loc)
        {
            speeds = new List<float>();
            angles = new List<float>();
        }

        public void forward()
        {
            if (angles.Exists(
                delegate(float ang)
                {
                    return ang == angle;
                }))
            {
                int i = angles.FindIndex(
                    delegate(float ang)
                    {
                        return ang == angle;
                    });
                speeds[i] += 0.1f;
                if (speeds[i] >= 5.0f)
                    speeds[i] = 5.0f;
            }
            else
            {
                angles.Add(angle);
                speeds.Add(0.1f);
            }
        }

        public override void Update(GameTime gameTime)
        {
            Console.WriteLine(MathHelper.ToDegrees(angle).ToString());
            for (int i = 0; i < speeds.Count; i++ )
            {
                transformToward(speeds[i], angles[i]);
                speeds[i] -= 0.02f;
                if (speeds[i] < 0.0f)
                {
                    speeds.RemoveAt(i);
                    angles.RemoveAt(i);
                }
            }
        }
    }
}