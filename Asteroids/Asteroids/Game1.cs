using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Asteroids
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        int winWidth = 1280, winHeight = 720;
        Input input;
        Collision collision;
        Entity ship;
        List<Entity> asteroids, bullets;
        Random rand;
        float delay;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            input = new Input(this);
            input.initGraphics(graphics);
            collision = new Collision(-50, -50, winWidth + 100, winHeight + 100);
            rand = new Random();
            asteroids = new List<Entity>();
            bullets = new List<Entity>();
            delay = 0;
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            ship = new Ship(Content.Load<Texture2D>("Assets//Ship"), 1, 2, 0, 1, new Vector2(winWidth / 2, winHeight / 2));
            for (int i = 0; i < 5; i++)
            {
                Entity ast = new Asteroid(Content.Load<Texture2D>("Assets//Asteroid"), 1, 1, 0, 1, new Vector2(rand.Next(winWidth), rand.Next(winHeight)), 2.0f, (float)rand.Next(360));
                //Entity ast = new Asteroid(Content.Load<Texture2D>("Assets//Asteroid"), 1, 1, 0, 1, new Vector2(0, 0));
                ((Asteroid)ast).init((float)rand.Next(3, 7));
                asteroids.Add(ast);
            }
            collision.add(ship);
            collision.add(asteroids);
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (input.keyPressed(Keys.Escape))
                this.Exit();
            if (input.keyHeld(Keys.Space) && delay >= 500)
            {
                Entity bul = new Bullet(Content.Load<Texture2D>("Assets//Bullet"), 1, 1, 0, 1, ship.position, 1.0f, MathHelper.ToDegrees(ship.angle));
                ((Bullet)bul).init(7.0f);
                bullets.Add(bul);
                collision.add(bul);
                delay = 0;
            }
            if (input.keyHeld(Keys.Right))
                ship.transform(0, 0, 5.0f);
            if (input.keyHeld(Keys.Left))
                ship.transform(0, 0, -5.0f);
            if (input.keyHeld(Keys.Up))
            {
                ((Ship)ship).forward();
                ship.newAnimation(0, 2);
                ship.animate();
            }
            if (input.keyPressed(Keys.Up))
            {
                ship.newAnimation(0, 1);
                ship.animate();
            }
            if (collision.hitLeftScreen(ship))
                ship.position.X = winWidth - ship.boundingBox.Width / 2;
            if (collision.hitRightScreen(ship))
                ship.position.X = 0;
            if (collision.hitTopScreen(ship))
                ship.position.Y = winHeight - ship.boundingBox.Height / 2;
            if (collision.hitBottomScreen(ship))
                ship.position.Y = 0;
            input.Update();
            ((Ship)ship).Update(gameTime);
            for(int i = 0; i < bullets.Count; i++)
            {
                ((Bullet)bullets[i]).Update(gameTime);
                if (collision.hitLeftScreen(bullets[i]))
                    bullets[i].position.X = winWidth - bullets[i].boundingBox.Width / 2;
                if (collision.hitRightScreen(bullets[i]))
                    bullets[i].position.X = 0 + bullets[i].boundingBox.Width / 2;
                if (collision.hitTopScreen(bullets[i]))
                    bullets[i].position.Y = winHeight - bullets[i].boundingBox.Height / 2;
                if (collision.hitBottomScreen(bullets[i]))
                    bullets[i].position.Y = 0 + bullets[i].boundingBox.Height / 2;
                if (((Bullet)bullets[i]).travelDis >= 1500.0f)
                    bullets.Remove(bullets[i]);
            }
            for(int i = 0; i < asteroids.Count; i++)
            {
                if (collision.hitLeftScreen(asteroids[i]))
                    asteroids[i].position.X = winWidth - asteroids[i].boundingBox.Width / 2;
                if (collision.hitRightScreen(asteroids[i]))
                    asteroids[i].position.X = 0 + asteroids[i].boundingBox.Width / 2;
                if (collision.hitTopScreen(asteroids[i]))
                    asteroids[i].position.Y = winHeight - asteroids[i].boundingBox.Height / 2;
                if (collision.hitBottomScreen(asteroids[i]))
                    asteroids[i].position.Y = 0 + asteroids[i].boundingBox.Height / 2;
                ((Asteroid)asteroids[i]).Update(gameTime);
                Entity ent = collision.collide(asteroids[i]);
                if (ent is Asteroid)
                {
                    //Console.WriteLine(ent.GetType().ToString());
                    float temp = ((Asteroid)ent).rotate;
                    ((Asteroid)ent).rotate = ((Asteroid)asteroids[i]).rotate;
                    ((Asteroid)asteroids[i]).rotate = temp;
                }
                if (ent is Bullet)
                {
                    collision.remove(ent);
                    collision.remove(asteroids[i]);
                    if (asteroids[i].scale == 2.0f)
                    {
                        Entity asteroid = new Asteroid(asteroids[i]);
                        asteroid.scale = 1.0f;
                        asteroid.angle += MathHelper.ToRadians(180);
                        asteroid.position += new Vector2(25, 25);
                        ((Asteroid)asteroid).init((float)rand.Next(3, 7));
                        asteroids.Add(asteroid);
                        asteroids[i].scale = 1.0f;
                    }
                    else
                        asteroids.Remove(asteroids[i]);
                    bullets.Remove(ent);
                }
            }
            delay += gameTime.ElapsedGameTime.Milliseconds;

            // TODO: Add your update logic here
            //asteroids[0].debug();
            //Console.WriteLine(Ship.angle.ToString());
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here
            spriteBatch.Begin();
            ship.Draw(spriteBatch);
            foreach (Entity ast in asteroids)
            {
                ast.Draw(spriteBatch);
            }
            foreach (Entity bul in bullets)
            {
                bul.Draw(spriteBatch);
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
