﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


class Collision
{
    
    private List<Entity> allEntities;
    private Rectangle screenBoundary;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="screenL">Boundary for left side of screen</param>
    /// <param name="screenT">Boundary for top of screen</param>
    /// <param name="screenR">Boundary for right side of screen</param>
    /// <param name="screenB">Boundary for bottom of screen</param>
    public Collision(int screenL, int screenT, int screenR, int screenB)
    {
        allEntities = new List<Entity>();
        screenBoundary = new Rectangle(screenL, screenT, screenR, screenB);
    }

    /// <summary>
    /// Add entity to list to do collision detection
    /// </summary>
    /// <param name="ent">Entity tobe added</param>
    public virtual void add(Entity ent)
    {
        allEntities.Add(ent);
    }

    /// <summary>
    /// Add a list of entities
    /// </summary>
    /// <param name="listEnt">List of entities tobe added</param>
    public virtual void add(List<Entity> listEnt)
    {
        allEntities.AddRange(listEnt);
    }

    /// <summary>
    /// Remove entity from list (it can't collide with anything anymore)
    /// </summary>
    /// <param name="ent">Entity tobe removed</param>
    public virtual void remove(Entity ent)
    {
        allEntities.Remove(ent);
    }

    /// <summary>
    /// Remove entity from list
    /// </summary>
    /// <param name="idx">Index of entity tobe removed</param>
    public virtual void remove(int idx)
    {
        allEntities.RemoveAt(idx);
    }

    /// <summary>
    /// Check of entity has moved off screen
    /// </summary>
    /// <param name="obj">Entity you want to check</param>
    /// <returns>True if off screen</returns>
    public virtual bool moveOffScreen(Entity obj)
    {
        return hitLeftScreen(obj) || hitTopScreen(obj) || hitRightScreen(obj) || hitBottomScreen(obj);
    }
    
    public virtual bool hitRightScreen(Entity obj)
    {
        return obj.boundingBox.Right > screenBoundary.Right;
    }

    public virtual bool hitLeftScreen(Entity obj)
    {
        return obj.boundingBox.Left < screenBoundary.Left;
    }

    public virtual bool hitTopScreen(Entity obj)
    {
        return obj.boundingBox.Top < screenBoundary.Top;
    }

    public virtual bool hitBottomScreen(Entity obj)
    {
        return obj.boundingBox.Bottom > screenBoundary.Bottom;
    }
    
    /// <summary>
    /// Check of entity is colliding with another entity
    /// </summary>
    /// <param name="obj">Entity being checked</param>
    /// <returns>Entity that collided with entity that was passed in</returns>
    public virtual Entity collide(Entity obj)
    {
        int idx = allEntities.FindIndex(
            delegate(Entity ent)
            {
                return ent.position == obj.position;
            });
        for (int i = 0; i < allEntities.Count; i++)
        {
            if (i != idx)
            {
                if (obj.boundingBox.Intersects(allEntities[i].boundingBox))
                    return intersectPixels(obj, allEntities[i]) ? allEntities[i] : null;
            }
        }
        return null;
    }


    private static bool intersectPixels(Entity obj1, Entity obj2)
    {
        // Calculate a matrix which transforms from A's local space into
        // world space and then into B's local space
        Matrix transformAToB = obj1.transformM * Matrix.Invert(obj2.transformM);

        // When a point moves in A's local space, it moves in B's local space with a
        // fixed direction and distance proportional to the movement in A.
        // This algorithm steps through A one pixel at a time along A's X and Y axes
        // Calculate the analogous steps in B:
        Vector2 stepX = Vector2.TransformNormal(Vector2.UnitX, transformAToB);
        Vector2 stepY = Vector2.TransformNormal(Vector2.UnitY, transformAToB);

        // Calculate the top left corner of A in B's local space
        // This variable will be reused to keep track of the start of each row
        Vector2 yPosInB = Vector2.Transform(Vector2.Zero, transformAToB);

        // For each row of pixels in A
        for (int yA = 0; yA < obj1.srcRect.Height; yA++)
        {
            // Start at the beginning of the row
            Vector2 posInB = yPosInB;

            // For each pixel in this row
            for (int xA = 0; xA < obj1.srcRect.Width; xA++)
            {
                // Round to the nearest pixel
                int xB = (int)Math.Round(posInB.X);
                int yB = (int)Math.Round(posInB.Y);

                // If the pixel lies within the bounds of B
                if (0 <= xB && xB < obj2.srcRect.Width &&
                    0 <= yB && yB < obj2.srcRect.Height)
                {
                    // Get the colors of the overlapping pixels
                    Color colorA = obj1.textureData[xA + yA * obj1.srcRect.Width];
                    Color colorB = obj2.textureData[xB + yB * obj2.srcRect.Width];

                    // If both pixels are not completely transparent,
                    if (colorA.A != 0 && colorB.A != 0)
                    {
                        // then an intersection has been found
                        return true;
                    }
                }

                // Move to the next pixel in the row
                posInB += stepX;
            }

            // Move to the next row
            yPosInB += stepY;
        }

        // No intersection found
        return false;
    }

    /// <summary>
    /// Check if space is ocuppied by an enity
    /// </summary>
    /// <param name="rect">Space being checked</param>
    /// <returns>Enitiy that is ocupping that space</returns>
    public virtual Entity collide(Rectangle rect)
    {
        for (int i = 0; i < allEntities.Count; i++)
        {
            if (rect.Left < allEntities[i].boundingBox.Right &&
                rect.Right > allEntities[i].boundingBox.Left &&
                rect.Top < allEntities[i].boundingBox.Bottom &&
                rect.Bottom > allEntities[i].boundingBox.Top)
                return allEntities[i];
        }
        return null;
    }
}